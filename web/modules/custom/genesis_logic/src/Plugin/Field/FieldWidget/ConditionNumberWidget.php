<?php

namespace Drupal\genesis_logic\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'number' widget.
 *
 * @FieldWidget(
 *   id = "condition_number",
 *   label = @Translation("Number field with display condition"),
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float"
 *   }
 * )
 */
class ConditionNumberWidget extends NumberWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'placeholder' => '',
        'condition_field_name' => '',
        'condition_field_value' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['placeholder'] = [
      '#type' => 'number',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#min' => 0,
      '#description' => t('Number that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];
    $element['condition_field_name'] = [
      '#type' => 'textfield',
      '#title' => t('Field name'),
      '#required' => TRUE,
      '#default_value' => $this->getSetting('condition_field_name'),
      '#description' => t('Field machine name (e.g. field_select) based on which the current field will be displayed'),
    ];
    $element['condition_field_value'] = [
      '#type' => 'textfield',
      '#title' => t('Field value'),
      '#required' => TRUE,
      '#default_value' => $this->getSetting('condition_field_value'),
      '#description' => t('Field value based on which the current field will be displayed'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = t('Related field name: @relatedFieldName', ['@relatedFieldName' => $this->getSetting('condition_field_name')]);
    $summary[] = t('Related field value: @relatedFieldValue', ['@relatedFieldValue' => $this->getSetting('condition_field_value')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state)['value'];
    $form_state->set('condition_field_name', $this->getSetting('condition_field_name'));
    $form_state->set('condition_field_value', $this->getSetting('condition_field_value'));

    $element += [
      '#element_validate' => [
        [get_class($this), 'validateElement'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="' . $this->getSetting('condition_field_name') . '"]' => ['value' => $this->getSetting('condition_field_value')],
        ],
      ],
    ];

    return ['value' => $element];
  }

  public static function validateElement(array $element, FormStateInterface $form_state) {
    $conditionFieldName = $form_state->get('condition_field_name');
    $selectedValue = $form_state->getValue($conditionFieldName)[0]['value'];

    if ($selectedValue != $form_state->get('condition_field_value')) {
      $form_state->setValueForElement($element, '');
    }
  }

}
